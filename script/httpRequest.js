var myForm = document.getElementById("myForm");

myForm.addEventListener("submit", function (e) {
  e.preventDefault();

  // get value from input
  var surName = document.getElementById("surName").value;
  var lastName = document.getElementById("lastName").value;
  var firstName = document.getElementById("firstName").value;
  var email = document.getElementById("email").value;
  var phone = document.getElementById("phone").value;
  var password = document.getElementById("password").value;

  var param =
    "surName=" +
    encodeURIComponent(surName) +
    "lastName=" +
    encodeURIComponent(lastName) +
    "firstName=" +
    encodeURIComponent(firstName) +
    "email=" +
    encodeURIComponent(email) +
    "phone=" +
    encodeURIComponent(phone) +
    "password=" +
    encodeURIComponent(password);

  function createXHR() {
    try {
      return new XMLHttpRequest();
    } catch (e) {}
    try {
      return new ActiveXObject("Msxml2.XMLHTTP.6.0");
    } catch (e) {}
    try {
      return new ActiveXObject("Msxml2.XMLHTTP.3.0");
    } catch (e) {}
    try {
      return new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {}
    try {
      return new ActiveXObject("Microsoft.XMLHTTP");
    } catch (e) {}
    alert("XMLHttpRequest non supporté");
    return null;
  }

  var XHRObj = createXHR();

  // Connect to server via POST
  XHRObj.open("POST", "data-processing.html", true);

  XHRObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

  XHRObj.onreadystatechange = function () {
    if (XHRObj.readyState == 4) {
      if (XHRObj.status == 200) {
        $("#subscribe_confirm").text(XHRObj.responseText);
      }
    }
  };

  XHRObj.send(param);
});
