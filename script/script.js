// toggle button for passwords
function displayPassword() {
  const passwordInput = document.getElementById("password");
  const toggleButton = document.getElementById("toggleButton");
  const passwordInput2 = document.getElementById("password2");
  const toggleButton2 = document.getElementById("toggleButton2");

  toggleButton.addEventListener("click", function (event) {
    event.preventDefault()
    if (passwordInput.type === "password") {
      passwordInput.type = "text";
    } else {
      passwordInput.type = "password";
    }
  });

  toggleButton2.addEventListener("click", function (event) {
    event.preventDefault()
    if (passwordInput2.type === "password") {
      passwordInput2.type = "text";
    } else {
      passwordInput2.type = "password";
    }
  });
}

// enable or disable button in html
function enableButtonVisual() {
  if (!submitBtn.disabled) {
    submitBtn.classList.remove("disabled-btn");
    submitBtn.classList.add("active-btn");
  } else {
    submitBtn.classList.add("disabled-btn");
    submitBtn.classList.remove("active-btn");
  }
}

// fucntion to validate surName
function validateSurname() {
  
  if (surNameInput.value.trim().length < 2) {
    surNameError.textContent = "Le surnom doit contenir au moins 2 caractères.";
    surNameError.classList.remove("hidden");
    return false;
  } else {
    surNameError.textContent = "";
    surNameError.classList.add("hidden");
    return true;
  }
}

// fucntion to validate lastName
function validateLastname() {
  const lastNameInput = document.getElementById("lastName");
  const lastNameError = document.getElementById("lastNameError");

  if (lastNameInput.value.trim().length < 2) {
    lastNameError.textContent = "Le nom de famille doit contenir au moins 2 caractères.";
    lastNameError.classList.remove("hidden");
    return false;
  } else {
    lastNameError.textContent = "";
    lastNameError.classList.add("hidden");
    return true;
  }
}

// fucntion to validate firstName
function validateFirstname() {
  const firstNameInput = document.getElementById("firstName");
  const firstNameError = document.getElementById("firstNameError");

  if (firstNameInput.value.trim().length < 2) {
    firstNameError.textContent = "Le prénom doit contenir au moins 2 caractères.";
    firstNameError.classList.remove("hidden");
    return false;
  } else {
    firstNameError.textContent = "";
    firstNameError.classList.add("hidden");
    return true;
  }
}

// fucntion to validate email
function validateEmail() {
  const emailInput = document.getElementById("email");
  const emailError = document.getElementById("emailError");
  const emailRegExp = /^[A-Za-z0-9+_.-]+@(.+)$/;

  if (!emailRegExp.test(emailInput.value)) {
    emailError.textContent = "Adresse e-mail invalide.";
    emailError.classList.remove("hidden");
    return false;
  } else {
    emailError.textContent = "";
    emailError.classList.add("hidden");
    return true;
  }
}

// fucntion to validate phone
function validatePhone() {
  const phoneInput = document.getElementById("phone");
  const phoneError = document.getElementById("phoneError");
  const phoneRegExp = /^((\+)33|0|0033)[1-9](\d{2}){4}$/;

  if (!phoneRegExp.test(phoneInput.value)) {
    phoneError.textContent = "Numéro de téléphone invalide.";
    phoneError.classList.remove("hidden");
    return false;
  } else {
    phoneError.textContent = "";
    phoneError.classList.add("hidden");
    return true;
  }
}

// fucntion to validate password
function validatePassword() {
  const passwordInput = document.getElementById("password");
  const passwordError = document.getElementById("passwordError");
  const passRegExp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/;

  if (!passRegExp.test(passwordInput.value)) {
    passwordError.textContent = "Le mot de passe doit contenir au moins 8 caractères, dont une lettre minuscule, une lettre majuscule et un chiffre.";
    passwordError.classList.remove("hidden");
    return false;
  } else {
    passwordError.textContent = "";
    passwordError.classList.add("hidden");
    return true;
  }
}

// function validate all input respect prior validate function
function isInputValid() {
  return (
    validateSurname() &&
    validateLastname() &&
    validateFirstname() &&
    validateEmail() &&
    validatePhone() &&
    validatePassword()
  );
}

// function to validate form and interact with the page
function validateForm() {
  let isInputNotEmpty = true; 
  const isCguChecked = cgu.checked; 

  // display color border if passwords are not identicals
  const isPasswordIdentical = passwordInput.value === passwordInput2.value;
  if (!isPasswordIdentical) {
    passwordInput.classList.add("redBorder");
    passwordInput2.classList.add("redBorder");
    isInputValid = false; 
  } else {
    passwordInput.classList.remove("redBorder");
    passwordInput2.classList.remove("redBorder");
  }

  // check if all function return true
  const validationResults = [
    validateSurname(),
    validateLastname(),
    validateFirstname(),
    validateEmail(),
    validatePhone(),
    validatePassword()
  ];

  // return all result as one value (true or false)
  const areInputsValid = validationResults.every((result) => result);

  // make sure error msg are empty
  const errorMessages = document.querySelectorAll(".error-message");
  errorMessages.forEach((errorMessage) => {
    errorMessage.textContent = "";
    errorMessage.classList.add("hidden");
  });

   // display error msg if function validate return false
   surNameInput.addEventListener("change", validateSurname);
   lastNameInput.addEventListener("change", validateLastname);
   firstNameInput.addEventListener("change", validateFirstname);
   emailInput.addEventListener("change", validateEmail);
   phoneInput.addEventListener("change", validatePhone);
   passwordInput.addEventListener("change", validatePassword);

   surNameInput.addEventListener("blur", validateSurname);
   lastNameInput.addEventListener("blur", validateLastname);
   firstNameInput.addEventListener("blur", validateFirstname);
   emailInput.addEventListener("blur", validateEmail);
   phoneInput.addEventListener("blur", validatePhone);
   passwordInput.addEventListener("blur", validatePassword);

   surNameInput.addEventListener("focus", validateSurname);
   lastNameInput.addEventListener("focus", validateLastname);
   firstNameInput.addEventListener("focus", validateFirstname);
   emailInput.addEventListener("focus", validateEmail);
   phoneInput.addEventListener("focus", validatePhone);
   passwordInput.addEventListener("focus", validatePassword);

  // Check if any of the required inputs is empty
  const requiredInputs = [surNameInput, lastNameInput, firstNameInput, emailInput];
  
  for (let input of requiredInputs) {
    if (input.value.trim() === "") {
      isInputNotEmpty = false;
      break;
    }
  }

  // Disable or enable the button based on all validation conditions
  submitBtn.disabled = !(isCguChecked && isPasswordIdentical && isInputNotEmpty && areInputsValid);
  enableButtonVisual();

  return isInputNotEmpty && isCguChecked && areInputsValid;
}

// listen to submit event, act as double security with prior code (submitBtn.disabled)
form.addEventListener("submit", function (event) {
  if (!validateForm()) {
    event.preventDefault();
  } else {
    console.log("formulaire envoyé");
    displayDiv.classList.remove("disabled");
    displayDiv.classList.add("enabled");
  }
});

// listen to input event on form
form.addEventListener("input", validateForm);

// call function to disable submitBtn on initial page loading
displayPassword();
validateForm();